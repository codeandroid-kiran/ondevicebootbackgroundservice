package me.codeandroid.ondevicebootbackgroundservices.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class databaseHelper extends SQLiteOpenHelper {

    Context context;
    private static final String TAG = "DBHelper";
    private static final int DATABASE_VERSION = 1;

    // Billing Database Name
    private static final String DATABASE_NAME = "Test.db";
    //BulkSms Table
    public static String TABLE_Bulksms = "BulkSMS";
    String CREATE_TABLE_Bulksms = "CREATE TABLE " + TABLE_Bulksms +  "( "
            + "SmsID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
            + "MobileNo TEXT NOT NULL DEFAULT ((0)), "
            + "Message TEXT NULL, "
            + "TypeSms INTEGER NOT NULL DEFAULT ((0)), "
            + "UniqueCode INTEGER NULL)";


    public databaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_Bulksms);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Bulksms);
        onCreate(db);
    }


    public static boolean isAnyInfoAvailable(Context ctx){
        boolean result = false;
        databaseHelper dbh = null;

        SQLiteDatabase db = null;
        try {
            dbh = new databaseHelper(ctx);
            db = dbh.getWritableDatabase();
            result = databaseHelper.is_any_info_available(db, ctx);
        } catch (Throwable e) {
            Log.e(TAG, "isAnyInfoAvailable(): Caught - " + e.getClass().getName(), e);
        } finally {
            if (null != db)
                db.close();
            if (null != dbh)
                dbh.close();
        }
        return result;
    }

    public static boolean is_any_info_available(SQLiteDatabase db, final Context ctx){
        boolean result = false;
        databaseHelper dbHelper = new databaseHelper(ctx);


        Cursor cInfo = db.rawQuery(
                "select * from " + TABLE_Bulksms, null);
        if(cInfo != null)
        {
            if(cInfo.moveToFirst()) {
                result = true;

                }while (cInfo.moveToNext());
            }

        if(cInfo != null)
            cInfo.close();
        return result;
    }

}
