package me.codeandroid.ondevicebootbackgroundservices.Services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import me.codeandroid.ondevicebootbackgroundservices.Database.databaseHelper;

public class service extends Service {
    public Runnable mRunnable = null;
    public service() {

    }


    private static final String TAG = "MyService";
    String mobNo, msg;
    int type, uniqueCode;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    public void onDestroy() {
        Toast.makeText(this, "My Service Stopped", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onDestroy");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final Handler mHandler = new Handler();
        final databaseHelper dbhelper = new databaseHelper(getApplicationContext());

        mRunnable = new Runnable() {
            @Override
            public void run() {

                boolean isInfoAvailable = dbhelper.isAnyInfoAvailable(getApplicationContext());
                Toast.makeText(getApplicationContext(), "Working : " + String.valueOf(isInfoAvailable) , Toast.LENGTH_LONG).show();
                mHandler.postDelayed(mRunnable, 10 * 1000);
            }
        };
        mHandler.postDelayed(mRunnable, 10 * 1000);




        return super.onStartCommand(intent, flags, startId);
    }

}
